#!/bin/sh

# exit when any command fails
set -e

alias curl="/host/bin/curl -s -L --retry 4 -f --retry-delay 60 --cacert /host/ca-certificates.crt"
GREETINGS="Thank you for contributing to freedesktop.org"

curl -O https://gitlab.freedesktop.org/freedesktop/helm-gitlab-infra/-/raw/main/runner-gating/users.txt

# ignore more checks for vips
if grep -qFx $GITLAB_USER_LOGIN users.txt
then
  rm users.txt
  echo "$GREETINGS"
  exit 0
fi

rm users.txt

# user is not a VIP,
# check if the project is part of a group => success
# this work either for direct commits in a project part of a group,
# but also works for Merge Requests open against a project part of a group
if curl -o /dev/null \
    https://gitlab.freedesktop.org/api/v4/groups/$CI_PROJECT_ROOT_NAMESPACE
then
  echo "$GREETINGS"
  exit 0
fi

echo "***************************************************************************************************************************"
echo ""
echo "Looks like you don't have enough privileges, please see https://gitlab.freedesktop.org/freedesktop/freedesktop/-/issues/438"
echo ""
echo "***************************************************************************************************************************"

# the following line needs to be commented to actually enforce the check
echo ""; echo "Skipping permission check for now"; exit 0

exit 1
