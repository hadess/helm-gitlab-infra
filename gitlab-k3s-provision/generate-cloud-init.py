#!/usr/bin/env python3
#
# Copyright © 2020 Daniel Stone
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice (including the next
# paragraph) shall be included in all copies or substantial portions of the
# Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.
#
# Authors: Daniel Stone <daniel@fooishbar.org>

import click
import os
import os.path
import requests
import yaml

# use literal rather than quoted encoding for block strings
# https://stackoverflow.com/a/33300001
def str_presenter(dumper, data):
  if len(data.splitlines()) > 1:  # check for multiline string
    return dumper.represent_scalar('tag:yaml.org,2002:str', data, style='|')
  return dumper.represent_scalar('tag:yaml.org,2002:str', data)

yaml.add_representer(str, str_presenter)


class MyDumper(yaml.Dumper):
    def increase_indent(self, flow=False, indentless=False):
        return super(MyDumper, self).increase_indent(flow, False)

def get_local_file(filename):
    return open(os.path.join(os.path.dirname(__file__), filename)).read()

def get_remote_uri(uri):
    get = requests.get(uri)
    if not get.ok:
        get.raise_for_status()
    return get.text

cloud_init = {
    'package-update': True,
    'package-upgrade': True,
    'apt': {
        'sources': {
            'buster-backports.list': {
                'source': 'deb http://deb.debian.org/debian buster-backports main contrib non-free',
            },
        },
    },
    'packages': [
        # HTTPS archive verification
        'debian-archive-keyring',
        'apt-transport-https',
        # sensible updates
        'needrestart',
        # ??
        'patch',
        # network
        'wireguard',
        'ufw',
        # ceph
        'lvm2',
        # fs
        'jq',
        'xfsprogs',
        # git
        'git',
        # for helmify
        'tree',
        # to check disk health
        'smartmontools',
        # daniels
        'zsh',
        'vim',
    ],
    'runcmd': [
        # disable swap in fstab
        "sed -i 's/^[^#]\\(.*swap.*\\)/#\\1/' /etc/fstab",

        # the upgrade by cloud-init doesn't upgrade the kernel
        'env DEBIAN_FRONTEND=noninteractive apt-get update',
        'env DEBIAN_FRONTEND=noninteractive apt-get -y dist-upgrade',

        # kernel-img.conf creates /boot/initrd.img when our grub expects /boot/initrd
        'ln -sf initrd.img /boot/initrd',

        # reboot to the new kernel if we need
        # we clean cloud-init data so that it re-runs next time
        'if [ -e /var/run/reboot-required ] ; then cloud-init clean ; rm /etc/apt/sources.list.d/buster-backports.list ; reboot ; sleep 60 ; fi',

        # switch to iptables-legacy, k3s doesn't support (yet) nftables
        'update-alternatives --set iptables /usr/sbin/iptables-legacy',
        'update-alternatives --set ip6tables /usr/sbin/ip6tables-legacy',

        # enable weekly trim
        'systemctl enable --now fstrim.timer',

        # configure ufw
        'ufw allow ssh',
        'ufw default deny incoming',
        'ufw default allow outgoing',
        'ufw default allow routed',
        'systemctl enable --now ufw',
        'ufw enable',
    ],
    'write_files': [
        {
            'path': '/etc/kernel-img.conf',
            'owner': 'root:root',
            'content': 'do_symlinks=Yes\nimage_dest=/boot',
        },
    ]
}


def add_k3s_master():
    KUSTOMIZE = 'v4.1.2'
    HELM = 'v3.5.4'
    HELMFILE = 'v0.138.7'
    cloud_init['write_files'].extend([
        {
            'path': '/root/k3s-config.yaml',
            'owner': 'root:root',
            'permissions': '0644',
            'content': get_local_file('k3s-config.yaml')
        },
    ])
    cloud_init['runcmd'].extend([
        # fetch kustomize
        f'curl -LO https://github.com/kubernetes-sigs/kustomize/releases/download/kustomize%2F{KUSTOMIZE}/kustomize_{KUSTOMIZE}_linux_amd64.tar.gz',
        f'tar xvf kustomize_{KUSTOMIZE}_linux_amd64.tar.gz -C /usr/local/bin kustomize',

        # install helm, helm diff, helmfile
        f'curl -LO https://get.helm.sh/helm-{HELM}-linux-amd64.tar.gz',
        f'tar xvf helm-{HELM}-linux-amd64.tar.gz -C /usr/local/bin --strip-components 1 linux-amd64/helm',

        'HOME=/root helm plugin install https://github.com/databus23/helm-diff --version master',

        f'curl -L -o /usr/local/bin/helmfile https://github.com/roboll/helmfile/releases/download/{HELMFILE}/helmfile_linux_amd64',
        'chmod +x /usr/local/bin/helmfile',
    ])


@click.command()
@click.option('--server', default=False, is_flag=True, help='prep the machine as a server node.')
@click.option('--plan', help='the kind of machine deployed.')
@click.option('--channel',
              type=click.STRING,
              default='v1.20',
              help='The release channel to pick the k3s binary from')
def main(server, channel, plan):
    if plan == 'c2.medium.x86':
        cloud_init['runcmd'].extend([
            # configure RAID for k3s storage
            # first disk is used for k3s images
            # other disks are used for k3s data in longhorn
            'mkdir -p /run/cloudinit',
            '''lsblk --json -b | jq -r '.blockdevices | map(select(.children == null and .size < 480000000000)) | map_values("/dev/" + .name) | join(" ")' > /run/cloudinit/FREE_SSD''',
            'echo FREE_SSD $(cat /run/cloudinit/FREE_SSD)',
            'for DEV in $(cat /run/cloudinit/FREE_SSD) ; do echo create partition on $DEV; echo type=83 | sfdisk $DEV; done',
            'mkfs.ext4 -F $(cat /run/cloudinit/FREE_SSD)1',
            'echo \'UUID="\'$(blkid -s UUID -o value $(cat /run/cloudinit/FREE_SSD)1)\'" /var/lib/rancher ext4 defaults 0 0\' | tee -a /etc/fstab',
            'mkdir -p /var/lib/rancher',
            'mount -a',
        ])
    elif plan == 's1.large.x86':
        cloud_init['runcmd'].extend([
            # first 480 disk is used for k3s images
            # HDD disks are used for k3s data
            'mkdir -p /run/cloudinit',
            '''lsblk --json -b | jq -r '.blockdevices | map(select(.children == null and .size < 500000000000)) | map_values("/dev/" + .name) | first' > /run/cloudinit/FREE_SSD''',
            'echo FREE_SSD $(cat /run/cloudinit/FREE_SSD)',
            'for DEV in $(cat /run/cloudinit/FREE_SSD) ; do echo create partition on $DEV; echo type=83 | sfdisk $DEV; done',
            'mkfs.ext4 -F $(cat /run/cloudinit/FREE_SSD)1',
            'echo \'UUID="\'$(blkid -s UUID -o value $(cat /run/cloudinit/FREE_SSD)1)\'" /var/lib/rancher ext4 defaults 0 0\' | tee -a /etc/fstab',
            'mkdir -p /var/lib/rancher',

            '''lsblk --json -b | jq -r '.blockdevices | map(select(.children == null and .size > 500000000000)) | .[:2] | map_values("/dev/" + .name) | join(" ")' > /run/cloudinit/FREE_HDD''',
            '''lsblk --json -b | jq -r '.blockdevices | map(select(.children == null and .size > 500000000000)) | .[:2] | map_values("/dev/" + .name + "1") | join(" ")' > /run/cloudinit/FREE_HDD_PART''',
            '''echo 2 > /run/cloudinit/COUNT_HDD''',
            'echo FREE_HDD $(cat /run/cloudinit/FREE_HDD)',
            'echo FREE_HDD_PART $(cat /run/cloudinit/FREE_HDD_PART)',
            'echo COUNT_HDD $(cat /run/cloudinit/COUNT_HDD)',
            'for DEV in $(cat /run/cloudinit/FREE_HDD) ; do echo create partition on $DEV; echo type=fd | sfdisk $DEV; done',
            'mdadm --create --verbose /dev/md0 --level=0 --raid-devices=$(cat /run/cloudinit/COUNT_HDD) $(cat /run/cloudinit/FREE_HDD_PART)',
            'mkfs -t xfs /dev/md0',
            'echo \'UUID="\'$(blkid -s UUID -o value /dev/md0)\'" /var/openebs/local xfs defaults 0 0\' | tee -a /etc/fstab',
            'mkdir -p /var/openebs/local',
            'mount -a',
        ])

    if server:
        add_k3s_master()

    print('#cloud-config')
    # these options make the result more human readable
    print(yaml.dump(cloud_init, Dumper=MyDumper, indent=2, width=9999, default_flow_style=False))
    print("")

if __name__ == '__main__':
    main()
