#!/usr/bin/env python3
#
# Copyright © 2021 Benjamin Tissoires
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice (including the next
# paragraph) shall be included in all copies or substantial portions of the
# Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.
#
# Authors: Benjamin Tissoires <benjamin.tissoires@gmail.com>

import click
import ipaddress
import json
import logging
import os
import paramiko  # for handling ssh
import packet
import random
import scp
import shlex
import shutil
import subprocess
import sys
import tempfile
import yaml

from attrs import define
from pathlib import Path
from typing import List


numeric_level = getattr(logging, "info".upper(), None)
logging.basicConfig(level=numeric_level)


@click.group()
@click.option(
    "--project-id",
    help="the project UUID on packet",
    default=os.getenv("PACKET_PROJECT_ID"),
)
@click.option(
    "--project-token",
    help="the project token on packet",
    default=os.getenv("PACKET_PROJECT_TOKEN"),
)
@click.pass_context
def fdo_infra(ctx, project_id, project_token):
    # ensure that ctx.obj exists and is a dict (in case `fdo_infra()` is called
    # by means other than the `if` block below)
    ctx.ensure_object(dict)
    ctx.obj["project_id"] = project_id
    ctx.obj["project_token"] = project_token


# based on https://github.com/hackersandslackers/paramiko-tutorial/blob/master/paramiko_tutorial/client.py
# MIT Licensed
class RemoteClient(object):
    """Client to interact with a remote host via SSH & SCP."""

    def __init__(
        self,
        host: str,
        user: str,
        deploy_key: bool = True,
        ssh_key_filepath: str = None,
    ):
        self.host = host
        self.user = user
        self.tmpdir = tempfile.TemporaryDirectory()
        self.ssh_key_filepath = ssh_key_filepath
        if deploy_key:
            if ssh_key_filepath is None:
                self._generate_ssh_key()
            self._upload_ssh_key()
        self._client = self.new_connection()
        self._scp = None

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.disconnect()
        self.tmpdir.cleanup()

    def new_connection(self):
        """Open connection to remote host."""
        try:
            client = paramiko.SSHClient()
            client.load_system_host_keys()
            client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            client.connect(
                self.host,
                username=self.user,
                key_filename=self.ssh_key_filepath,
                timeout=5000,
            )
            return client
        except paramiko.auth_handler.AuthenticationException as e:
            logging.error(
                f"Authentication failed: did you remember to create an SSH key? {e}"
            )
            raise e

    def new_scp(self) -> scp.SCPClient:
        conn = self.new_connection()
        return scp.SCPClient(conn.get_transport())

    @property
    def scp(self):
        if self._scp is None:
            self._scp = self.new_scp()

        return self._scp

    def _get_ssh_key(self):
        """Fetch locally stored SSH key."""
        try:
            self.ssh_key = paramiko.RSAKey.from_private_key_file(self.ssh_key_filepath)
            logging.info(f"Found SSH key at self {self.ssh_key_filepath}")
            return self.ssh_key
        except paramiko.auth_handler.SSHException as e:
            logging.error(e)

    def _generate_ssh_key(self):
        self.ssh_key_filepath = str(Path(self.tmpdir.name) / "id_fdo")
        try:
            subprocess.run(
                [
                    "ssh-keygen",
                    "-a",
                    "100",
                    "-t",
                    "ed25519",
                    "-f",
                    self.ssh_key_filepath,
                    "-C",
                    f"k3sup@{self.host}",
                    "-q",
                    "-N",
                    "",
                ],
                check=True,
            )
            logging.info(f"{self.ssh_key_filepath} created")
        except FileNotFoundError as error:
            logging.error(error)

    def _upload_ssh_key(self):
        try:
            with subprocess.Popen(
                shlex.split(
                    f"ssh-copy-id -i {self.ssh_key_filepath}.pub {self.user}@{self.host}"
                ),
                stdout=subprocess.PIPE,
            ) as proc:
                logging.info(proc.stdout.read())
                if proc.wait() != 0:
                    raise RuntimeError("ssh-copy-id failed")
            logging.info(f"{self.ssh_key_filepath} uploaded to {self.host}")
        except FileNotFoundError as error:
            logging.error(error)

    def disconnect(self):
        """Close SSH & SCP connection."""
        # remove our generated keypair
        self.execute_commands([f"sed -i '/k3sup@{self.host}/d' ~/.ssh/authorized_keys"])
        self._client.close()
        if self._scp is not None:
            self._scp.close()

    def bulk_upload(self, files: List[str], remote_path: str):
        """
        Upload multiple files to a remote directory.
        :param files: List of local files to be uploaded.
        :type files: List[str]
        """
        try:
            self.scp.put(files, remote_path=remote_path)
            logging.info(
                f"Finished uploading {len(files)} files to {remote_path} on {self.host}"
            )
        except scp.SCPException as e:
            raise e

    def download_file(self, file: str):
        """Download file from remote host."""
        self.scp.get(file)

    def execute_commands(self, commands: List[str]):
        """
        Execute multiple commands in succession.
        :param commands: List of unix commands as strings.
        :type commands: List[str]
        """
        compact = len(commands) > 10
        if compact:
            command = ";".join(commands)
            commands = [
                f"bash -c '{command}'",
            ]
        for cmd in commands:
            stdin, stdout, stderr = self._client.exec_command(cmd)
            stdout.channel.recv_exit_status()
            response = stdout.readlines()
            cmd = f"{cmd[:50]}..." if compact else cmd
            for line in response:
                logging.info(f"INPUT: {cmd} | OUTPUT: {line.rstrip()}")
            errs = stderr.readlines()
            for line in errs:
                logging.warning(f"INPUT: {cmd} | OUTPUT: {line.rstrip()}")

    def execute_command(self, command: str):
        """
        Execute one command.
        :param command: the unix command as a string.
        :type command: str
        """
        stdin, stdout, stderr = self._client.exec_command(command)
        status = stdout.channel.recv_exit_status()
        return status, stdout, stderr


def check_k3sup():
    """check if k3sup is installed"""
    installed = shutil.which("k3sup") is not None
    if not installed:
        logging.error(
            """
    k3sup could not be found
    please use the following to install k3sup:

    curl -sLS https://get.k3sup.dev | sh
    sudo install k3sup /usr/local/bin/"""
        )
        sys.exit(1)


def load_config(project_id: str, project_token: str):
    if not project_id:
        logging.error(
            "project-id missing, either set PACKET_PROJECT_ID environment variable, either use --project-id"
        )
        sys.exit(1)

    if not project_token:
        logging.error(
            "project-token missing, either set PACKET_PROJECT_TOKEN environment variable, either use --project-token"
        )
        sys.exit(1)

    with open("config.yaml") as f:
        config = yaml.safe_load(f)

    config["manager"] = packet.Manager(auth_token=project_token)
    config["project-id"] = project_id
    config["project-token"] = project_token

    return config


def _get_packet_device(name, config):
    if "devices" not in config:
        params = {"per_page": 50}

        config["devices"] = config["manager"].list_devices(
            project_id=config["project-id"], params=params
        )

    our_devices = [d for d in config["devices"] if d.hostname == name]

    if not len(our_devices):
        logging.error(f"device '{name}' doesn't exist")
        sys.exit(1)
    elif len(our_devices) > 1:
        logging.error(f"more than one device with '{name}' exist, can not continue")
        sys.exit(1)

    return our_devices[0]


def _get_ip(name, config, version, public):
    device = _get_packet_device(name, config)

    return [
        ip
        for ip in device.ip_addresses
        if ip["address_family"] == version and ip["public"] == public
    ][0]["address"]


def public_ipv4(name, config):
    return _get_ip(name, config, 4, public=True)


def public_ipv6(name, config):
    return _get_ip(name, config, 6, public=True)


def private_ipv4(name, config):
    return _get_ip(name, config, 4, public=False)


def get_plan(name, config):
    return _get_packet_device(name, config)["plan"]["name"]


def setup_server(name, config, ssh_con):
    private_ip = private_ipv4(name, config)
    _public_ipv6 = public_ipv6(name, config)

    # upload the config file
    ssh_con.bulk_upload(["k3s-config.yaml"], "/root")

    ssh_con.execute_commands(
        [
            # replace the placeholders values
            f"sed -i 's/SERVER_PRIVATE_IP/{private_ip}/' /root/k3s-config.yaml",
            f"sed -i 's/SERVER_PUBLIC_IPV6/{_public_ipv6}/' /root/k3s-config.yaml",
            # setup /etc/hosts
            f"sed -i 's/\(127.0.0.1.*localhost\).*{name}/\\1\\n{private_ip}\\t{name}/' /etc/hosts",
            # remove leftover EIP
            f"ip a del {config['control_plane_eip']}/32 dev lo",
        ]
    )


def _cluster_init(name, config):
    public_ip = public_ipv4(name, config)
    private_ip = private_ipv4(name, config)
    plan = get_plan(name, config)

    kubeconfig = Path.home() / ".kube" / name

    # bug in k3sup
    # https://github.com/alexellis/k3sup/pull/317
    if kubeconfig.exists():
        kubeconfig.unlink()

    logging.info(f"connecting to {name}")
    with RemoteClient(user="root", host=public_ip) as ssh_con:
        setup_server(name, config, ssh_con)

        # install k3s through k3sup
        command = f'''k3sup install --ip {public_ip} \
              --user root \
              --sudo false \
              --ssh-key {ssh_con.ssh_key_filepath} \
              --tls-san {config['control_plane_eip']} \
              --cluster \
              --k3s-channel {config['channel']} \
              --no-extras \
              --merge \
              --local-path {kubeconfig.absolute()} \
              --context=admin@{name} \
              --k3s-extra-args "--config /root/k3s-config.yaml --node-label fdo.plan={plan}"'''

        try:
            subprocess.run(shlex.split(command), check=True)
        except subprocess.CalledProcessError as e:
            print("stdout", e.stdout)
            print("stderr", e.stderr)
            raise e

        # use the elastic IP in the config file
        subprocess.run(
            shlex.split(
                f"sed -i 's/{public_ip}/{config['control_plane_eip']}/' {kubeconfig.absolute()}"
            ),
            check=True,
        )

        secret = Path(ssh_con.tmpdir.name) / "metal-cloud-config.yaml"
        with open(secret, "w") as f:
            f.write(
                f"""---
apiVersion: v1
kind: Secret
metadata:
  name: metal-cloud-config
  namespace: kube-system
stringData:
  cloud-sa.json: |
    {{
      "loadbalancer": "kube-vip://",
      "eipTag": "",
      "apiKey": "{config["project-token"]}",
      "projectID": "{config["project-id"]}"
    }}"""
            )
        ssh_con.bulk_upload([secret], "/root")

        # deploy the various k8s charts
        tmpdir = tempfile.TemporaryDirectory()
        helm_commands = [
            f"kubectl apply -f /root/{secret.name}",
            f"rm /root/{secret.name}",
            f"git clone -b {config['deployment']['branch']} {config['deployment']['url']} {tmpdir.name}",
            f"KUBECONFIG=/etc/rancher/k3s/k3s.yaml helmfile -e packet-HA --file {tmpdir.name}/gitlab-k3s-provision/deploy/helmfile.yaml apply",
            f"rm -rf {tmpdir.name}",
        ]

        ssh_con.execute_commands(helm_commands)


@define
class Node:
    name: str
    device: object
    is_server: bool = False


@define
class UFWRule:
    desc: str
    port: int = None
    proto: str = None
    from_: str = None
    interface: str = None

    @classmethod
    def from_ufw(cls, ufw, from_=None):
        from_ = from_ if from_ else ufw.from_
        return cls(ufw.desc, ufw.port, ufw.proto, from_, ufw.interface)

    def build_command(self):
        proto_rule = f" proto {self.proto}" if self.proto else ""
        port_rule = f" to any port {self.port}" if self.port else ""
        from_rule = f" from {self.from_}" if self.from_ else ""
        interface_rule = f" in on {self.interface}" if self.interface else ""
        return f"ufw allow{interface_rule}{proto_rule}{port_rule}{from_rule}"

    def description(self, from_=None):
        from_rule = f" from {from_.name}" if from_ else ""
        return f"{self.desc}{from_rule}"


def _get_current_nodes(eip_ssh_con, config):
    status, stdout, stderr = eip_ssh_con.execute_command(
        f"kubectl get nodes -l node-role.kubernetes.io/control-plane=true -o name"
    )
    nodes = []
    for line in stdout.readlines():
        name = line.rstrip().split("/")[1]
        server = Node(name, _get_packet_device(name, config), True)
        nodes.append(server)

    status, stdout, stderr = eip_ssh_con.execute_command(f"kubectl get nodes -o name")
    for line in stdout.readlines():
        name = line.rstrip().split("/")[1]
        # skip servers, they were added above
        if name in [n.name for n in nodes]:
            continue

        node = Node(name, _get_packet_device(name, config), False)
        nodes.append(node)

    return nodes


def _get_ufw_rules(dest_node, nodes, config, src_node=None):
    ufw_rules = [
        UFWRule("SSH in", 22, "tcp"),
        UFWRule("wg kilo in", 51821, "udp"),
        UFWRule("k3s cluster cidr in", from_="10.40.0.0/16"),
        UFWRule("k3s service cidr in", from_="10.41.0.0/16"),
        UFWRule("allow kilo to anything", from_="10.0.0.0/8", interface="kilo0"),
    ]
    # from https://rancher.com/docs/k3s/latest/en/installation/installation-requirements/
    ufw_common_rules = [
        # UFWRule("Required only for Flannel VXLAN", 8472, 'udp'),
        UFWRule("Required only for Flannel Wireguard backend", 51820, "udp"),
        # UFWRule("Required only for Flannel Wireguard backend with IPv6", 51821, 'udp'),
        UFWRule("Kubelet metrics", 10250, "tcp"),
    ]
    ufw_server_rules = [
        UFWRule("Kubernetes API Server", 6443, "tcp"),
    ]
    ufw_server_only_rules = [
        UFWRule("Required only for HA with embedded etcd", 2379, "tcp"),
        UFWRule("Required only for HA with embedded etcd", 2380, "tcp"),
    ]

    commands = [ufw for ufw in ufw_rules] if src_node is None else []

    for ufw in ufw_common_rules:
        for node in nodes:
            if node == dest_node:
                continue
            if src_node and node != src_node and dest_node != src_node:
                continue

            commands.append(UFWRule.from_ufw(ufw, private_ipv4(node.name, config)))
            commands.append(UFWRule.from_ufw(ufw, public_ipv4(node.name, config)))

    if dest_node.is_server:
        for ufw in ufw_server_rules:
            for node in nodes:
                if src_node and node != src_node and dest_node != src_node:
                    continue

                commands.append(UFWRule.from_ufw(ufw, private_ipv4(node.name, config)))
                commands.append(UFWRule.from_ufw(ufw, public_ipv4(node.name, config)))

        for ufw in ufw_server_only_rules:
            for node in nodes:
                if node == dest_node or not node.is_server:
                    continue
                if src_node and node != src_node and dest_node != src_node:
                    continue

                commands.append(UFWRule.from_ufw(ufw, private_ipv4(node.name, config)))
                commands.append(UFWRule.from_ufw(ufw, public_ipv4(node.name, config)))

    return commands


def _refresh_ufw(name, config, new_node=None):
    logging.info(f"connecting to {config['control_plane_eip']}")
    with RemoteClient(
        user="root", host=config["control_plane_eip"], deploy_key=True
    ) as eip_ssh_con:
        nodes = _get_current_nodes(eip_ssh_con, config)
        if new_node is not None:
            nodes.append(new_node)

        targets = (
            nodes
            if name is None
            else [Node(name, _get_packet_device(name, config), "server" in name)]
        )

        for target in targets:
            commands = _get_ufw_rules(target, nodes, config, new_node)
            ssh_commands = [c.build_command() for c in commands]

            logging.info(f"updating iptables on {target.name}")
            current_desc = None
            for c in commands:
                desc = c.description()
                if current_desc != desc:
                    logging.info(f"# {desc}")
                    current_desc = desc
                logging.info(c.build_command())

            with RemoteClient(
                user="root",
                host=public_ipv4(target.name, config),
                ssh_key_filepath=eip_ssh_con.ssh_key_filepath,
            ) as serv_ssh_con:
                serv_ssh_con.execute_commands(ssh_commands)


def _server_join(name, config):
    node = Node(name, _get_packet_device(name, config), True)
    public_ip = public_ipv4(name, config)
    private_ip = private_ipv4(name, config)
    plan = get_plan(name, config)

    # refresh all ufw rules to all machines on the cluster to allow
    # the new node to connect to it
    _refresh_ufw(None, config, new_node=node)

    # first connect to the elastip IP to install the ssh key
    logging.info(f"connecting to {config['control_plane_eip']}")
    with RemoteClient(
        user="root", host=config["control_plane_eip"], deploy_key=True
    ) as eip_ssh_con:

        logging.info(f"connecting to {name}")
        with RemoteClient(
            user="root", host=public_ip, ssh_key_filepath=eip_ssh_con.ssh_key_filepath
        ) as ssh_con:
            setup_server(name, config, ssh_con)

            # install k3s through k3sup
            command = f"""k3sup join --ip {public_ip} \
               --server \
               --ssh-key {ssh_con.ssh_key_filepath} \
               --server-ip {config['control_plane_eip']} \
               --k3s-extra-args "--config /root/k3s-config.yaml --node-label fdo.plan={plan}" \
               --k3s-channel {config['channel']}"""

            try:
                subprocess.run(shlex.split(command), check=True)
            except subprocess.CalledProcessError as e:
                print("stdout", e.stdout)
                print("stderr", e.stderr)
                raise e


def _join(name, config):
    node = Node(name, _get_packet_device(name, config), False)
    public_ip = public_ipv4(name, config)
    private_ip = private_ipv4(name, config)
    plan = get_plan(name, config)

    # refresh all ufw rules to all machines on the cluster to allow
    # the new node to connect to it
    _refresh_ufw(None, config, new_node=node)

    # first connect to the elastip IP to install the ssh key
    logging.info(f"connecting to {config['control_plane_eip']}")
    with RemoteClient(
        user="root", host=config["control_plane_eip"], deploy_key=True
    ) as eip_ssh_con:

        logging.info(f"connecting to {name}")
        with RemoteClient(
            user="root", host=public_ip, ssh_key_filepath=eip_ssh_con.ssh_key_filepath
        ) as ssh_con:
            # install k3s through k3sup
            command = f"""k3sup join --ip {public_ip} \
               --ssh-key {ssh_con.ssh_key_filepath} \
               --server-ip {config['control_plane_eip']} \
               --k3s-extra-args "--node-label fdo.plan={plan}" \
               --k3s-channel {config['channel']}"""

            subprocess.run(shlex.split(command), check=True)


def _add_peer(name, server_name, config):
    public_ip = public_ipv4(server_name, config)

    # generate a public/private wg key pair
    with subprocess.Popen(["wg", "genkey"], stdout=subprocess.PIPE) as proc:
        private_key = proc.stdout.read().rstrip()

    with subprocess.Popen(
        ["wg", "pubkey"], stdout=subprocess.PIPE, stdin=subprocess.PIPE
    ) as proc:
        public_key, err = proc.communicate(input=private_key)
        public_key = public_key.rstrip()

    logging.info(f"connecting to {server_name}")
    with RemoteClient(
        user="root",
        host=public_ip,
    ) as ssh_con:
        # Retrieve the public key of the server
        status, stdout, stderr = ssh_con.execute_command(f"wg show kilo0 public-key")
        server_key = stdout.read().rstrip()

        # Find a non-assigned IP in the 10.6.0.0/24 range
        status, stdout, stderr = ssh_con.execute_command(
            f"kubectl get -A peers.kilo.squat.ai -o json"
        )
        peers = json.load(stdout)

        cur_ips = []
        for item in peers["items"]:
            if item["metadata"]["name"] == name:
                logging.error(f"peer {name} is already used, aborting")
                sys.exit(1)
            cur_ips.extend(item["spec"]["allowedIPs"])

        assigned_ip = None
        net = ipaddress.ip_network(config["kilo_peers"]["cidr"])
        while assigned_ip is None:
            test_ip = random.choice(list(net.hosts()))
            if test_ip not in cur_ips:
                assigned_ip = test_ip

        # write the kilo peer config
        peer = Path(ssh_con.tmpdir.name) / f"{name}-peer.yaml"
        with open(peer, "w") as f:
            f.write(
                f"""---
apiVersion: kilo.squat.ai/v1alpha1
kind: Peer
metadata:
  name: {name}
spec:
  allowedIPs:
  - {assigned_ip}/32
  publicKey: {public_key.decode()}
  persistentKeepalive: 10"""
            )

        # upload and apply it
        ssh_con.bulk_upload([peer], "/root")
        ssh_con.execute_commands(
            [f"kubectl apply -f /root/{peer.name}", f"rm /root/{peer.name}"]
        )

        # retrieve the config of the cluster
        with open("k3s-config.yaml") as f:
            cluster = yaml.safe_load(f)

        # write the wireguard config file to stdout
        print(
            f"""[Interface]
Address = {assigned_ip}
PrivateKey = {private_key.decode()}
MTU = 1350

[Peer]
AllowedIPs = {cluster["cluster-cidr"]}, {cluster["service-cidr"]}, {config["control_plane_eip"]}/32, 10.8.0.1/32
Endpoint = {public_ip}:51821
PersistentKeepalive = 10
PublicKey = {server_key.decode()}"""
        )


@fdo_infra.command(name="cluster-init")
@click.argument("name")
@click.pass_context
def cluster_init(ctx, name: str):
    check_k3sup()
    config = load_config(ctx.obj["project_id"], ctx.obj["project_token"])

    logging.info(config)
    _cluster_init(name, config)


@fdo_infra.command(name="server-join")
@click.argument("name")
@click.pass_context
def server_join(ctx, name: str):
    check_k3sup()
    config = load_config(ctx.obj["project_id"], ctx.obj["project_token"])

    logging.info(config)
    _server_join(name, config)


@fdo_infra.command(name="join")
@click.argument("name")
@click.pass_context
def join(ctx, name: str):
    check_k3sup()
    config = load_config(ctx.obj["project_id"], ctx.obj["project_token"])

    logging.info(config)
    _join(name, config)


@fdo_infra.command(name="refresh-ufw")
@click.argument("name", required=False)
@click.pass_context
def refresh_ufw(ctx, name: str):
    config = load_config(ctx.obj["project_id"], ctx.obj["project_token"])

    logging.info(config)
    _refresh_ufw(name, config)


@fdo_infra.command(name="add-peer")
@click.option(
    "--server",
    help="one of the control plane server name",
    default="fdo-k3s-server-1",
)
@click.argument("name")
@click.pass_context
def add_peer(ctx, name: str, server: str):
    config = load_config(ctx.obj["project_id"], ctx.obj["project_token"])

    logging.info(config)
    _add_peer(name, server, config)


def main(*args, **kwargs):
    fdo_infra(obj={}, *args, **kwargs)


if __name__ == "__main__":
    main()
