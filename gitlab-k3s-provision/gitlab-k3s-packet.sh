#!/bin/sh

# you must set:
#  - $PACKET_PROJECT_ID to the fd.o CI Packet project
#  - $PACKET_DEVICE_NAME to the desired device name (e.g. fdo-k3s-5)

PLAN=c2.medium.x86

metal device create --project-id $PACKET_PROJECT_ID --hostname $PACKET_DEVICE_NAME --plan $PLAN --metro ny --operating-system debian_11 --userdata "$(./generate-cloud-init.py --plan $PLAN $@)"
